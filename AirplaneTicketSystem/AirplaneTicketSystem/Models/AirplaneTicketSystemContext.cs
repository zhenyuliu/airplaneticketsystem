﻿using System.Data.Entity;

namespace AirplaneTicketSystem.Models
{
    public class AirplaneTicketSystemContext : DbContext
    {
        public AirplaneTicketSystemContext() : base("name=AirplaneTicketSystemConnection") { }

        public DbSet<DealEntity> Deals { get; set; }
        public DbSet<TicketEntity> Tickets { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<CityEntity> Cities { get; set; }
    }
}