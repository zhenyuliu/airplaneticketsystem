﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AirplaneTicketSystem.Models.ViewModels
{
    public class TicketViewModel
    {
        public int Id { get; set; }

        [StringLength(32)]
        [Required]
        public string FlightNumber { get; set; }

        [Required]
        public string StartPlace { get; set; }

        [Required]
        public string EndPlace { get; set; }

        public string Country { get; set; }

        [Required]
        public DateTime ExpireDate { get; set; }

        [Required]
        public string ClassType { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public int Remain { get; set; }
    }
}