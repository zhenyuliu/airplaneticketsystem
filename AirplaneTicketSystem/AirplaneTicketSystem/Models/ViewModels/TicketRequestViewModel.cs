﻿using System;

namespace AirplaneTicketSystem.Models.ViewModels
{
    public class TicketRequestViewModel
    {
        public string StartPlace { get; set; }

        public string EndPlace { get; set; }

        public DateTime ExpireDate { get; set; }

        public string ClassType { get; set; }
    }
}