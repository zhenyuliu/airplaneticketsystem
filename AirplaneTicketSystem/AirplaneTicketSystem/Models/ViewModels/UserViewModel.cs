﻿using System.ComponentModel.DataAnnotations;

namespace AirplaneTicketSystem.Models.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }

        [StringLength(255)]
        [Required]
        public string FirstName { get; set; }

        [StringLength(255)]
        [Required]
        public string LastName { get; set; }

        public int Age { get; set; }
    }
}