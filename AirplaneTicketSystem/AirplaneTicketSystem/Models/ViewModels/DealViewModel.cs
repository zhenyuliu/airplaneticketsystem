﻿using System.ComponentModel.DataAnnotations;

namespace AirplaneTicketSystem.Models.ViewModels
{
    public class DealViewModel
    {
        public int Id { get; set; }

        [Required]
        public int TicketAmount { get; set; }

        public TicketViewModel Ticket { get; set; }

    }
}