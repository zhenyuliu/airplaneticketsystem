﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AirplaneTicketSystem.Models
{
    [Table("Deal")]
    public class DealEntity : IEntity
    {
        public int Id { get; set; }

        [Required]
        public int TicketId { get; set; }

        [Required]
        public int TicketAmount { get; set; }

        public virtual TicketEntity Ticket { get; set; }
    }
}