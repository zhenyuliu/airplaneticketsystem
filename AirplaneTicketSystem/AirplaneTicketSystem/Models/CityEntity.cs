﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AirplaneTicketSystem.Models
{
    [Table("City")]
    public class CityEntity : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }
}