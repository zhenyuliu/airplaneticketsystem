namespace AirplaneTicketSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveSubmitDateFromDeal : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Deal", "SubmitDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deal", "SubmitDate", c => c.DateTime(nullable: false));
        }
    }
}
