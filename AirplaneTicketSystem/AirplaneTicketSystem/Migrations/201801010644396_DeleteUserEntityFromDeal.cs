namespace AirplaneTicketSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteUserEntityFromDeal : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Deal", "UserId", "dbo.User");
            DropIndex("dbo.Deal", new[] { "UserId" });
            DropColumn("dbo.Deal", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deal", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Deal", "UserId");
            AddForeignKey("dbo.Deal", "UserId", "dbo.User", "Id", cascadeDelete: true);
        }
    }
}
