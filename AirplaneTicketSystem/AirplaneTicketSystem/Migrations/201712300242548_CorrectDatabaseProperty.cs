namespace AirplaneTicketSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrectDatabaseProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ticket", "Remain", c => c.Int(nullable: false));
            DropColumn("dbo.Ticket", "Remian");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Ticket", "Remian", c => c.Int(nullable: false));
            DropColumn("dbo.Ticket", "Remain");
        }
    }
}
