namespace AirplaneTicketSystem.Migrations
{
    using AirplaneTicketSystem.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AirplaneTicketSystem.Models.AirplaneTicketSystemContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AirplaneTicketSystem.Models.AirplaneTicketSystemContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            if (!context.Tickets.Any())
            {
                context.Tickets.AddRange(new List<TicketEntity>
                {
                    new TicketEntity
                    {
                        Id = 1,
                        FlightNumber = "VA899",
                        StartPlace = "Melbourne",
                        EndPlace = "Sydney",
                        Country = "Australia",
                        ExpireDate = new DateTime(2018, 1, 30),
                        ClassType = "Economy",
                        Price = 371.5m,
                        Remain = 300
                    },
                    new TicketEntity
                    {
                        Id = 2,
                        FlightNumber = "VA899",
                        StartPlace = "Melbourne",
                        EndPlace = "Sydney",
                        Country = "Australia",
                        ExpireDate = new DateTime(2018, 1, 30),
                        ClassType = "Business",
                        Price = 750.5m,
                        Remain = 10
                    },
                    new TicketEntity
                    {
                        Id = 3,
                        FlightNumber = "JS772",
                        StartPlace = "Melbourne",
                        EndPlace = "Adelaide",
                        Country = "Australia",
                        ExpireDate = new DateTime(2018, 1, 30),
                        ClassType = "Economy",
                        Price = 191.7m,
                        Remain = 150
                    },
                    new TicketEntity
                    {
                        Id = 4,
                        FlightNumber = "JS772",
                        StartPlace = "Melbourne",
                        EndPlace = "Adelaide",
                        Country = "Australia",
                        ExpireDate = new DateTime(2018, 1, 30),
                        ClassType = "Bussiness",
                        Price = 450.7m,
                        Remain = 10
                    },
                    new TicketEntity
                    {
                        Id = 5,
                        FlightNumber = "JS903",
                        StartPlace = "Melbourne",
                        EndPlace = "Beijing",
                        Country = "China",
                        ExpireDate = new DateTime(2018, 1, 30),
                        ClassType = "Economy",
                        Price = 1657.95m,
                        Remain = 600
                    },
                    new TicketEntity
                    {
                        Id = 6,
                        FlightNumber = "JS903",
                        StartPlace = "Melbourne",
                        EndPlace = "Beijing",
                        Country = "China",
                        ExpireDate = new DateTime(2018, 1, 30),
                        ClassType = "Bussiness",
                        Price = 3800.95m,
                        Remain = 20
                    }
                });
            }

            if (!context.Users.Any())
            {
                context.Users.AddRange(new List<UserEntity>
                {
                    new UserEntity
                    {
                        Id = 1,
                        FirstName = "Xiaoming",
                        LastName = "Zhang",
                        Age = 20
                    },
                    new UserEntity
                    {
                        Id = 2,
                        FirstName = "Alex",
                        LastName = "Peter",
                        Age = 35
                    },
                });
            }
            if (!context.Cities.Any())
            {
                context.Cities.AddRange(new List<CityEntity>
                {
                    new CityEntity
                    {
                        Id = 1,
                        Name = "Melbourne"
                    },
                    new CityEntity
                    {
                        Id = 2,
                        Name = "Sydney"
                    },
                    new CityEntity
                    {
                        Id = 3,
                        Name = "Adelaide"
                    },
                    new CityEntity
                    {
                        Id = 4,
                        Name = "Beijing"
                    }
                });
            }
            if (!context.Deals.Any())
            {
                context.Deals.AddRange(new List<DealEntity>
                {
                    new DealEntity
                    {
                        Id = 1,
                        TicketId = 1,
                        TicketAmount = 1
                    },
                    new DealEntity
                    {
                        Id = 2,
                        TicketId = 5,
                        TicketAmount = 1
                    },
                });
            }
        }
    }
}
