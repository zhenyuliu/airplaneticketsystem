namespace AirplaneTicketSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddThreeRelatedTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deal",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TicketId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        TicketAmount = c.Int(nullable: false),
                        SubmitDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ticket", t => t.TicketId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.TicketId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Ticket",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FlightNumber = c.String(nullable: false, maxLength: 32),
                        StartPlace = c.String(nullable: false),
                        EndPlace = c.String(nullable: false),
                        Country = c.String(),
                        ExpireDate = c.DateTime(nullable: false),
                        ClassType = c.String(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remian = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 255),
                        LastName = c.String(nullable: false, maxLength: 255),
                        Age = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Deal", "UserId", "dbo.User");
            DropForeignKey("dbo.Deal", "TicketId", "dbo.Ticket");
            DropIndex("dbo.Deal", new[] { "UserId" });
            DropIndex("dbo.Deal", new[] { "TicketId" });
            DropTable("dbo.User");
            DropTable("dbo.Ticket");
            DropTable("dbo.Deal");
        }
    }
}
