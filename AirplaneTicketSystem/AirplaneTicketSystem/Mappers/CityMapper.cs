﻿using AirplaneTicketSystem.Models;
using AirplaneTicketSystem.Models.ViewModels;

namespace AirplaneTicketSystem.Mappers
{
    public static class CityMapper
    {
        public static CityViewModel FromEntity(CityEntity entity)
        {
            return new CityViewModel
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public static void ToEntity(CityViewModel vm, CityEntity entity)
        {
            entity.Id = vm.Id;
            entity.Name = vm.Name;
        }
    }
}