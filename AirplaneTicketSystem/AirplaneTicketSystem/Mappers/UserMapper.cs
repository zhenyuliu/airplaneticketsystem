﻿using AirplaneTicketSystem.Models;
using AirplaneTicketSystem.Models.ViewModels;

namespace AirplaneTicketSystem.Mappers
{
    public static class UserMapper
    {
        public static UserViewModel FromEntity(UserEntity entity)
        {
            return new UserViewModel
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Age = entity.Age
            };
        }

        public static void ToEntity(UserViewModel vm, UserEntity entity)
        {
            entity.FirstName = vm.FirstName;
            entity.LastName = vm.LastName;
            entity.Age = vm.Age;
        }
    }
}