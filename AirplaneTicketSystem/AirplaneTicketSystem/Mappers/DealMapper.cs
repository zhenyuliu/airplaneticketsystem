﻿using AirplaneTicketSystem.Models;
using AirplaneTicketSystem.Models.ViewModels;

namespace AirplaneTicketSystem.Mappers
{
    public static class DealMapper
    {
        public static DealViewModel FromEntity(DealEntity entity)
        {
            return new DealViewModel
            {
                Id = entity.Id,
                TicketAmount = entity.TicketAmount,
                Ticket = TicketMapper.FromEntity(entity.Ticket)
            };
        }

        public static void ToEntity(DealViewModel vm, DealEntity entity)
        {
            entity.TicketId = vm.Ticket.Id;
            entity.TicketAmount = vm.TicketAmount;
        }
    }
}