﻿using AirplaneTicketSystem.Models;
using AirplaneTicketSystem.Models.ViewModels;

namespace AirplaneTicketSystem.Mappers
{
    public static class TicketMapper
    {
        public static TicketViewModel FromEntity(TicketEntity entity)
        {
            return new TicketViewModel
            {
                Id = entity.Id,
                FlightNumber = entity.FlightNumber,
                StartPlace = entity.StartPlace,
                EndPlace = entity.EndPlace,
                Country = entity.Country,
                ExpireDate = entity.ExpireDate,
                ClassType = entity.ClassType,
                Price = entity.Price,
                Remain = entity.Remain
            };
        }

        public static void ToEntity(TicketViewModel vm, TicketEntity entity)
        {
            entity.FlightNumber = vm.FlightNumber;
            entity.StartPlace = vm.StartPlace;
            entity.EndPlace = vm.EndPlace;
            entity.Country = vm.Country;
            entity.ExpireDate = vm.ExpireDate;
            entity.ClassType = vm.ClassType;
            entity.Price = vm.Price;
            entity.Remain = vm.Remain;

        }
    }
}