﻿using AirplaneTicketSystem.Models;
using AirplaneTicketSystem.Repositories;
using AirplaneTicketSystem.Services;
using Autofac;
using Autofac.Integration.WebApi;
using System.Web.Http;

namespace AirplaneTicketSystem.App_Start
{
    public class AutofacConfig
    {
        public static void BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(typeof(WebApiApplication).Assembly);

            // Register Service
            builder.RegisterType<TicketService>().As<ITicketService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<DealService>().As<IDealService>();
            builder.RegisterType<CityService>().As<ICityService>();

            // Register Repository
            builder.RegisterType<TicketRepository>().As<ITicketRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<DealRepository>().As<IDealRepository>();
            builder.RegisterType<CityRepository>().As<ICityRepository>();

            // Register Context
            builder.RegisterType<AirplaneTicketSystemContext>().As<AirplaneTicketSystemContext>();

            var container = builder.Build();

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}