﻿using AirplaneTicketSystem.Models;
using System.Data.Entity;
using System.Linq;

namespace AirplaneTicketSystem.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class, IEntity
    {
        private readonly AirplaneTicketSystemContext _context;

        private readonly DbSet<TEntity> _entities;

        public RepositoryBase(AirplaneTicketSystemContext context)
        {
            _context = context;
            _entities = context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return _entities;
        }

        public TEntity Get(int id)
        {
            return _entities.Find(id);
        }

        public void Create(TEntity entity)
        {
            _entities.Add(entity);
            _context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            _entities.Attach(entity);
            _context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var toDelete = _entities.Find(id);
            _entities.Remove(toDelete);
            _context.SaveChanges();
        }
    }
}