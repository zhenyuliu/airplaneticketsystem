﻿using AirplaneTicketSystem.Models;
using System;
using System.Linq;

namespace AirplaneTicketSystem.Repositories
{
    public interface ITicketRepository : IRepositoryBase<TicketEntity>
    {
        IQueryable<TicketEntity> Get(string StartPlace, string EndPlace, string ClassType, DateTime ExpireDate);
    }
}
