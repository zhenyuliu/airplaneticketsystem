﻿using AirplaneTicketSystem.Models;

namespace AirplaneTicketSystem.Repositories
{
    public class DealRepository : RepositoryBase<DealEntity>, IDealRepository
    {
        public DealRepository(AirplaneTicketSystemContext context) : base(context)
        {

        }
    }
}