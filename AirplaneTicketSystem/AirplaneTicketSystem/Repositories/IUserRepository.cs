﻿using AirplaneTicketSystem.Models;

namespace AirplaneTicketSystem.Repositories
{
    public interface IUserRepository : IRepositoryBase<UserEntity>
    {
    }
}
