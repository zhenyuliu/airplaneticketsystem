﻿using AirplaneTicketSystem.Models;

namespace AirplaneTicketSystem.Repositories
{
    public class UserRepository : RepositoryBase<UserEntity>, IUserRepository
    {
        public UserRepository(AirplaneTicketSystemContext context) : base(context)
        {

        }
    }
}