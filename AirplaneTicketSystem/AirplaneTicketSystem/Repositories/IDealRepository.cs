﻿using AirplaneTicketSystem.Models;

namespace AirplaneTicketSystem.Repositories
{
    public interface IDealRepository : IRepositoryBase<DealEntity>
    {
    }
}
