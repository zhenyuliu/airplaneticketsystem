﻿using AirplaneTicketSystem.Models;
using System;
using System.Linq;

namespace AirplaneTicketSystem.Repositories
{
    public class TicketRepository : RepositoryBase<TicketEntity>, ITicketRepository
    {
        public TicketRepository(AirplaneTicketSystemContext context) : base(context)
        {

        }

        public IQueryable<TicketEntity> Get(string StartPlace, string EndPlace, string ClassType, DateTime ExpireDate)
        {
            var tickets = GetAll()
                .Where(t =>
                (string.IsNullOrEmpty(StartPlace) || t.StartPlace == StartPlace)
                 && (string.IsNullOrEmpty(EndPlace) || t.EndPlace == EndPlace)
                 && (string.IsNullOrEmpty(ExpireDate.ToString()) || t.ExpireDate == ExpireDate)
                 && (string.IsNullOrEmpty(ClassType) || t.ClassType == ClassType)
            );
            return tickets;
        }
    }
}