﻿using AirplaneTicketSystem.Models;

namespace AirplaneTicketSystem.Repositories
{
    public class CityRepository : RepositoryBase<CityEntity>, ICityRepository
    {
        public CityRepository(AirplaneTicketSystemContext context) : base(context)
        {

        }
    }
}