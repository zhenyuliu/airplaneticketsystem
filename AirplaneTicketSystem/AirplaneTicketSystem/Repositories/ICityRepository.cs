﻿using AirplaneTicketSystem.Models;

namespace AirplaneTicketSystem.Repositories
{
    public interface ICityRepository : IRepositoryBase<CityEntity>
    {

    }
}