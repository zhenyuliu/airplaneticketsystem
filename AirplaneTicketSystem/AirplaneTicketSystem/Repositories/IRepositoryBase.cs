﻿using AirplaneTicketSystem.Models;
using System.Linq;

namespace AirplaneTicketSystem.Repositories
{
    public interface IRepositoryBase<T> where T : class, IEntity
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Create(T entity);
        void Update(T entity);
        void Delete(int id);
    }
}
