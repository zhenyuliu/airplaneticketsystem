﻿using AirplaneTicketSystem.Models.ViewModels;
using AirplaneTicketSystem.Services;
using System;
using System.Web.Http;

namespace AirplaneTicketSystem.Controllers
{
    public class SearchTerms
    {
        public string StartPlace { get; set; }
        public string EndPlace { get; set; }
        public DateTime ExpireDate { get; set; }
        public string ClassType { get; set; }
    }

    public class TicketsController : ApiController
    {
        private readonly ITicketService _ticketService;

        public TicketsController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpGet]
        [Route("api/tickets")]
        public IHttpActionResult Get(string StartPlace, string EndPlace, string ClassType, DateTime ExpireDate)
        {
            var targetTickets = _ticketService.Get(StartPlace, EndPlace, ClassType, ExpireDate);
            return Ok(targetTickets);

        }

        //public IHttpActionResult Get()
        //{
        //    var tickets = _ticketService.GetAll();
        //    return Ok(tickets);
        //}

        public IHttpActionResult Get(int id)
        {
            var ticket = _ticketService.GetById(id);
            if (ticket == null)
            {
                return NotFound();
            }

            return Ok(ticket);
        }

        public IHttpActionResult Post([FromBody]TicketViewModel vm)
        {
            if (vm.Id > 0)
            {
                return BadRequest("The ticket can not be created!");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input!");
            }

            try
            {
                _ticketService.Create(vm);
                return Created("/api/tickets", vm);
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }

        public IHttpActionResult Put(int id, [FromBody] TicketViewModel vm)
        {
            if (id == 0 || id != vm.Id)
            {
                return BadRequest("This ticket can not be created.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input.");
            }

            try
            {
                _ticketService.Update(vm);
                return Ok();
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                _ticketService.Delete(id);
                return Ok();
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
