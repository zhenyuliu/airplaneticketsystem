﻿using AirplaneTicketSystem.Models.ViewModels;
using AirplaneTicketSystem.Services;
using System.Web.Http;

namespace AirplaneTicketSystem.Controllers
{
    public class DealsController : ApiController
    {
        private readonly IDealService _dealService;

        public DealsController(IDealService dealService)
        {
            _dealService = dealService;
        }

        public IHttpActionResult Get()
        {
            var deals = _dealService.GetAll();
            return Ok(deals);
        }

        public IHttpActionResult Get(int id)
        {
            var deal = _dealService.GetById(id);
            if (deal == null)
            {
                return NotFound();
            }

            return Ok(deal);
        }

        public IHttpActionResult Post([FromBody]DealViewModel vm)
        {
            if (vm.Id > 0)
            {
                return BadRequest("The deal can not be created!");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input!");
            }

            try
            {
                _dealService.Create(vm);
                return Created("/api/deals", vm);
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }

        public IHttpActionResult Put(int id, [FromBody] DealViewModel vm)
        {
            if (id == 0 || id != vm.Id)
            {
                return BadRequest("This deal can not be created.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input.");
            }

            try
            {
                _dealService.Update(vm);
                return Ok();
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                _dealService.Delete(id);
                return Ok();
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
