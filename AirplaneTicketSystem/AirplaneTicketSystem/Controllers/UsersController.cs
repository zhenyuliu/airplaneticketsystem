﻿using AirplaneTicketSystem.Models.ViewModels;
using AirplaneTicketSystem.Services;
using System.Web.Http;

namespace AirplaneTicketSystem.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        public IHttpActionResult Get()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }

        public IHttpActionResult Get(int id)
        {
            var user = _userService.GetById(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        public IHttpActionResult Post([FromBody]UserViewModel vm)
        {
            if (vm.Id > 0)
            {
                return BadRequest("The user can not be created!");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input!");
            }

            try
            {
                _userService.Create(vm);
                return Created("/api/users", vm);
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }

        public IHttpActionResult Put(int id, [FromBody] UserViewModel vm)
        {
            if (id == 0 || id != vm.Id)
            {
                return BadRequest("This user can not be created.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input.");
            }

            try
            {
                _userService.Update(vm);
                return Ok();
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                _userService.Delete(id);
                return Ok();
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
