﻿using AirplaneTicketSystem.Models.ViewModels;
using AirplaneTicketSystem.Services;
using System.Web.Http;

namespace AirplaneTicketSystem.Controllers
{
    public class CitiesController : ApiController
    {
        private readonly ICityService _cityService;

        public CitiesController(ICityService cityService)
        {
            _cityService = cityService;
        }

        public IHttpActionResult Get()
        {
            var cities = _cityService.GetAll();
            return Ok(cities);
        }

        public IHttpActionResult Get(int id)
        {
            var city = _cityService.GetById(id);
            if (city == null)
            {
                return NotFound();
            }

            return Ok(city);
        }

        public IHttpActionResult Post([FromBody]CityViewModel vm)
        {
            if (vm.Id > 0)
            {
                return BadRequest("The city can not be created!");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input!");
            }

            try
            {
                _cityService.Create(vm);
                return Created("/api/cities", vm);
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }

        public IHttpActionResult Put(int id, [FromBody] CityViewModel vm)
        {
            if (id == 0 || id != vm.Id)
            {
                return BadRequest("This city can not be created.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input.");
            }

            try
            {
                _cityService.Update(vm);
                return Ok();
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                _cityService.Delete(id);
                return Ok();
            }
            catch (System.Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
