﻿using AirplaneTicketSystem.Mappers;
using AirplaneTicketSystem.Models;
using AirplaneTicketSystem.Models.ViewModels;
using AirplaneTicketSystem.Repositories;
using System;
using System.Collections.Generic;

namespace AirplaneTicketSystem.Services
{
    public class TicketService : ITicketService
    {
        private readonly ITicketRepository _ticketRepository;

        public TicketService(ITicketRepository ticketRepository)
        {
            _ticketRepository = ticketRepository;
        }

        public IEnumerable<TicketViewModel> GetAll()
        {
            var entities = _ticketRepository.GetAll();
            var vm = new List<TicketViewModel>();

            foreach (var entity in entities)
            {
                var ticketVm = TicketMapper.FromEntity(entity);
                vm.Add(ticketVm);
            }

            return vm;
        }

        public IEnumerable<TicketViewModel> Get(string StartPlace, string EndPlace, string ClassType, DateTime ExpireDate)
        {
            var entities = _ticketRepository.Get(StartPlace, EndPlace, ClassType, ExpireDate);
            var vm = new List<TicketViewModel>();
            if (entities == null)
            {
                return null;
            }
            foreach (var entity in entities)
            {
                var ticketVm = TicketMapper.FromEntity(entity);
                vm.Add(ticketVm);
            }

            return vm;
        }

        public TicketViewModel GetById(int id)
        {
            var entity = _ticketRepository.Get(id);

            if (entity == null)
            {
                return null;
            }

            return TicketMapper.FromEntity(entity);
        }

        public void Create(TicketViewModel vm)
        {
            var entity = new TicketEntity();
            TicketMapper.ToEntity(vm, entity);
            _ticketRepository.Create(entity);
            vm.Id = entity.Id;
        }

        public void Update(TicketViewModel vm)
        {
            var entity = _ticketRepository.Get(vm.Id);
            TicketMapper.ToEntity(vm, entity);
            _ticketRepository.Update(entity);
        }

        public void Delete(int id)
        {
            _ticketRepository.Delete(id);
        }
    }
}