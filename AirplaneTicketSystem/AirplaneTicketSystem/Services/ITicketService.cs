﻿using AirplaneTicketSystem.Models.ViewModels;
using System;
using System.Collections.Generic;

namespace AirplaneTicketSystem.Services
{
    public interface ITicketService
    {
        IEnumerable<TicketViewModel> GetAll();

        IEnumerable<TicketViewModel> Get(string StartPlace, string EndPlace, string ClassType, DateTime ExpireDate);

        TicketViewModel GetById(int id);

        void Create(TicketViewModel ticketModel);

        void Update(TicketViewModel ticketModel);

        void Delete(int id);
    }
}
