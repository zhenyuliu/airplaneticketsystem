﻿using AirplaneTicketSystem.Models.ViewModels;
using System.Collections.Generic;

namespace AirplaneTicketSystem.Services
{
    public interface IUserService
    {
        IEnumerable<UserViewModel> GetAll();

        UserViewModel GetById(int id);

        void Create(UserViewModel ticketModel);

        void Update(UserViewModel ticketModel);

        void Delete(int id);
    }
}
