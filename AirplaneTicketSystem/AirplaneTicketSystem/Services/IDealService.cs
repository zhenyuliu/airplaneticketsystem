﻿using AirplaneTicketSystem.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirplaneTicketSystem.Services
{
    public interface IDealService
    {
        IEnumerable<DealViewModel> GetAll();

        DealViewModel GetById(int id);

        void Create(DealViewModel ticketModel);

        void Update(DealViewModel ticketModel);

        void Delete(int id);
    }
}
