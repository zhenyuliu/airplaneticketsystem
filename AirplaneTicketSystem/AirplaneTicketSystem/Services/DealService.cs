﻿using AirplaneTicketSystem.Mappers;
using AirplaneTicketSystem.Models;
using AirplaneTicketSystem.Models.ViewModels;
using AirplaneTicketSystem.Repositories;
using System.Collections.Generic;

namespace AirplaneTicketSystem.Services
{
    public class DealService : IDealService
    {
        private readonly IDealRepository _dealRepository;

        public DealService(IDealRepository dealRepository)
        {
            _dealRepository = dealRepository;
        }

        public IEnumerable<DealViewModel> GetAll()
        {
            var entities = _dealRepository.GetAll();
            var vm = new List<DealViewModel>();

            foreach (var entity in entities)
            {
                var dealVm = DealMapper.FromEntity(entity);
                vm.Add(dealVm);
            }

            return vm;
        }

        public DealViewModel GetById(int id)
        {
            var entity = _dealRepository.Get(id);

            if (entity == null)
            {
                return null;
            }

            return DealMapper.FromEntity(entity);
        }

        public void Create(DealViewModel vm)
        {
            var entity = new DealEntity();
            DealMapper.ToEntity(vm, entity);
            _dealRepository.Create(entity);
            vm.Id = entity.Id;
        }

        public void Update(DealViewModel vm)
        {
            var entity = _dealRepository.Get(vm.Id);
            DealMapper.ToEntity(vm, entity);
            _dealRepository.Update(entity);
        }

        public void Delete(int id)
        {
            _dealRepository.Delete(id);
        }
    }
}