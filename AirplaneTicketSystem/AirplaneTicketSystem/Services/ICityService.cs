﻿using AirplaneTicketSystem.Models.ViewModels;
using System.Collections.Generic;

namespace AirplaneTicketSystem.Services
{
    public interface ICityService
    {
        IEnumerable<CityViewModel> GetAll();

        CityViewModel GetById(int id);

        void Create(CityViewModel cityModel);

        void Update(CityViewModel cityModel);

        void Delete(int id);
    }
}