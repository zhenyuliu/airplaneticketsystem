﻿using AirplaneTicketSystem.Mappers;
using AirplaneTicketSystem.Models;
using AirplaneTicketSystem.Models.ViewModels;
using AirplaneTicketSystem.Repositories;
using System.Collections.Generic;

namespace AirplaneTicketSystem.Services
{
    public class CityService : ICityService
    {
        private readonly ICityRepository _cityRepository;

        public CityService(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        public IEnumerable<CityViewModel> GetAll()
        {
            var entities = _cityRepository.GetAll();
            var vm = new List<CityViewModel>();

            foreach (var entity in entities)
            {
                var cityVm = CityMapper.FromEntity(entity);
                vm.Add(cityVm);
            }
            return vm;
        }

        public CityViewModel GetById(int id)
        {
            var entity = _cityRepository.Get(id);

            if (entity == null)
            {
                return null;
            }

            return CityMapper.FromEntity(entity);
        }

        public void Create(CityViewModel vm)
        {
            var entity = new CityEntity();
            CityMapper.ToEntity(vm, entity);
            _cityRepository.Create(entity);
            vm.Id = entity.Id;
        }

        public void Update(CityViewModel vm)
        {
            var entity = _cityRepository.Get(vm.Id);
            CityMapper.ToEntity(vm, entity);
            _cityRepository.Update(entity);
        }

        public void Delete(int id)
        {
            _cityRepository.Delete(id);
        }
    }
}