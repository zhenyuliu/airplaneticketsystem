﻿using AirplaneTicketSystem.Mappers;
using AirplaneTicketSystem.Models;
using AirplaneTicketSystem.Models.ViewModels;
using AirplaneTicketSystem.Repositories;
using System.Collections.Generic;

namespace AirplaneTicketSystem.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<UserViewModel> GetAll()
        {
            var entities = _userRepository.GetAll();
            var vm = new List<UserViewModel>();

            foreach (var entity in entities)
            {
                var userVm = UserMapper.FromEntity(entity);
                vm.Add(userVm);
            }

            return vm;
        }

        public UserViewModel GetById(int id)
        {
            var entity = _userRepository.Get(id);

            if (entity == null)
            {
                return null;
            }

            return UserMapper.FromEntity(entity);
        }

        public void Create(UserViewModel vm)
        {
            var entity = new UserEntity();
            UserMapper.ToEntity(vm, entity);
            _userRepository.Create(entity);
            vm.Id = entity.Id;
        }

        public void Update(UserViewModel vm)
        {
            var entity = _userRepository.Get(vm.Id);
            UserMapper.ToEntity(vm, entity);
            _userRepository.Update(entity);
        }

        public void Delete(int id)
        {
            _userRepository.Delete(id);
        }
    }
}