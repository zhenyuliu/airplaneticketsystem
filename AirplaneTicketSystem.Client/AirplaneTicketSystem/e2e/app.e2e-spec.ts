import { AirplaneTicketSystemPage } from './app.po';

describe('airplane-ticket-system App', function() {
  let page: AirplaneTicketSystemPage;

  beforeEach(() => {
    page = new AirplaneTicketSystemPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
