export interface ITicket {
    id: number;
    flightNumber: string;
    startPlace: string;
    endPlace: string;
    country: string;
    expireDate: Date;
    classType: string;
    price: number;
    remain: number;
}