import  { ITicket } from './ticket';
import  { IUser } from './user';

export interface IDeal {
    id: number;
    ticket: ITicket;
    ticketAmount: number;  
}