export interface ISearchTerm {
    startPlace: string;
    endPlace: string;
    expireDate: Date;
    classType: string;
}