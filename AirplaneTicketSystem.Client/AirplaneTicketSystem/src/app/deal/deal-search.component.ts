import { Component, OnInit } from '@angular/core';
import { DealService } from 'app/services/deal.service';
import { IDeal } from 'app/models/deal';
import { ITicket } from 'app/models/ticket';
import { TicketService } from 'app/services/ticket.service';

import { Router, NavigationExtras } from '@angular/router';
import { ISearchTerm } from 'app/models/searchTerms';
import { ICity } from 'app/models/city';
import { CityService } from 'app/services/city.service';

@Component({
  templateUrl: './deal-search.component.html'
})
export class DealSearchComponent implements OnInit {
  pageTtile: string = "Search deal";
  deal: IDeal;
  tickets: ITicket[];
  targetTickets: ITicket[];
  cities: ICity[];
  ticketAmount: number;
  searchTerms: ISearchTerm;
  errorMessage: string;
  isSearchFlag: boolean = false;

  constructor(
    private dealService: DealService,
    private ticketService: TicketService,
    private cityService: CityService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initDeal();
  }

  initDeal(){
    this.deal = <IDeal> {};
    this.searchTerms = <ISearchTerm> {};
    this.cityService.getCities().then(cities => this.cities = cities );
  }

  searchTickets(){
    this.setSeachFlag();
    this.ticketService.getTicketsByParamTerm(this.searchTerms)
    .then(t => this.targetTickets = t)
    .catch(this.handleError);
  }

  setSeachFlag(){
    this.isSearchFlag = true;
  }

  addTicket(ticket: ITicket){
    let navigationExtras: NavigationExtras = {queryParams:{}};
    navigationExtras.queryParams = ticket;
    this.router.navigate(['/deals/new'], navigationExtras);
  }

  typeSelected(selectedType: string) {
    this.searchTerms[selectedType] = selectedType;
  }

  private handleError(){
    this.errorMessage = "Error occurred.";
  }

}
