import { Component, OnInit } from '@angular/core';
import { DealService } from 'app/services/deal.service';
import { IDeal } from 'app/models/deal';
import { TicketService } from 'app/services/ticket.service';
import { ITicket } from 'app/models/ticket';

@Component({

  templateUrl: './deal-list.component.html'

})
export class DealListComponent implements OnInit {
  pageTitle: string = "Deal List";
  deals: IDeal[];
  deleteDealId: number = null;
  errorMessage: string;

  constructor(private dealService: DealService, private ticketService: TicketService) { }

  ngOnInit() {
    this.refreshDeals();
  }

  confirmCancel() {
    this.deleteDealId = null;
  }

  confirmDelete() {
    let deleteDeal = <IDeal>{};
    let ticket = <ITicket>{};
    this.dealService.GetDealById(this.deleteDealId).then(d => {
      deleteDeal = d;
      ticket = d.ticket;
      ticket.remain += deleteDeal.ticketAmount;
    }).then(a => {
      this.ticketService.updateTicket(ticket);
      this.dealService.DeleteDeal(deleteDeal).then(p => this.refreshDeals());
    })
    .catch(reason => {
      this.errorMessage = "An error occurred!";
    });     
  }

  setDeleteDealId(deal: IDeal){
    this.deleteDealId = deal.id;
  }

  totalPrice(count: number, price: number){
    return count * price;
  }

  private refreshDeals(){
    this.dealService.GetDeals().then(deals => this.deals = deals);
  }
}
