import { Component, OnInit } from '@angular/core';
import { TicketService } from 'app/services/ticket.service';
import { DealService } from 'app/services/deal.service';
import { Router } from '@angular/router';
import { ITicket } from 'app/models/ticket';
import { ActivatedRoute } from '@angular/router';
import { IDeal } from 'app/models/deal';

@Component({
  templateUrl: './deal-new.component.html'
})
export class DealNewComponent implements OnInit {
  targetTicket: ITicket;
  ticketAmount: number;

  constructor(
    private dealService: DealService,
    private ticketService: TicketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.targetTicket = <ITicket> {};
    this.route.queryParams.subscribe(params => {
      this.targetTicket.id = params["id"];
      this.targetTicket.flightNumber = params["flightNumber"];
      this.targetTicket.startPlace = params["startPlace"];
      this.targetTicket.endPlace = params["endPlace"];
      this.targetTicket.expireDate = params["expireDate"];
      this.targetTicket.country = params["country"];
      this.targetTicket.classType = params["classType"];
      this.targetTicket.price = params["price"];
      this.targetTicket.remain = params["remain"];  
  });
  }

  ngOnInit() {
  }

  createDeal(ticket: ITicket, amount: number){
    let deal = <IDeal>{};
    if(this.isEnoughRemain(amount, ticket.remain)){
      deal.ticket = ticket;
      deal.ticketAmount = amount;
      ticket.remain -= amount;
      this.ticketService.updateTicket(ticket);
      this.dealService.CreateDeal(deal).then(()=>{
        this.goBack();
      })
    } else {
      alert("Not enough tickets remain.")
      this.goBack();
    }
  }

  isEnoughRemain(wantAmount: number, remain: number): Boolean {
    return remain >= wantAmount;
  }

  goBack(){
    this.router.navigate(['/deals']);
  }
}
