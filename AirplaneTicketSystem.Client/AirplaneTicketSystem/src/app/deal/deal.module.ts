import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { RouterModule } from "@angular/router";
import { DealListComponent } from "app/deal/deal-list.component";
import { DealService } from "app/services/deal.service";
import { DealSearchComponent } from './deal-search.component';
import { FormsModule } from "@angular/forms";
import { DealNewComponent } from './deal-new.component';
import { CityService } from "app/services/city.service";
import { TicketService } from "app/services/ticket.service";



@NgModule({
    declarations: [DealListComponent, DealSearchComponent, DealNewComponent],
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: "deals",
                component: DealListComponent
            },
            {
                path: "",
                component: DealSearchComponent
            },
            {
                path: "deals/new",
                component: DealNewComponent
            }
        ])
    ],
    providers: [
        DealService, CityService, TicketService
    ]
})
export class DealModule {

}