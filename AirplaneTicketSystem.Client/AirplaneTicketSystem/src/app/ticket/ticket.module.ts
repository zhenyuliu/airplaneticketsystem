import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { RouterModule, Routes } from "@angular/router";
import { TicketListsComponent } from "./ticket-lists.component";
import { TicketService } from "app/services/ticket.service";
import { TicketNewComponent } from "./ticket-new.component";
import { FormsModule } from "@angular/forms";

@NgModule({
    declarations: [TicketListsComponent, TicketNewComponent],
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule.forChild([
            {
                path: 'tickets', 
                component: TicketListsComponent
            }
        ]),
        FormsModule
        
    ],
    providers: [
        TicketService
    ]
})
export class TicketModule{

}