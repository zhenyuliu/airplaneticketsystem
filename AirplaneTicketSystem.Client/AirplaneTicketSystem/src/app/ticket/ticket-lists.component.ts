import { Component, OnInit } from '@angular/core';
import { ITicket } from 'app/models/ticket';
import { TicketService } from 'app/services/ticket.service';

@Component({

  templateUrl: './ticket-lists.component.html'
})
export class TicketListsComponent implements OnInit {
  pageTitle: string = "Ticket List";
  tickets: ITicket[];

  constructor(private ticketService: TicketService) { }

  ngOnInit() {
    this.ticketService.getTickets().then(t => this.tickets = t);
  }

}
