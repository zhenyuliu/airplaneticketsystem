import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { Http, Headers, URLSearchParams, RequestOptions, Response } from "@angular/http";

@Injectable()
export class CityService {
    private _apiUrl = `${environment.apiDomain}/cities`;
    private _header = new Headers({ "Content-Type": "application/json" });

    constructor(private http: Http){}

    getCities(){
        return this.http.get(this._apiUrl)
        .toPromise()
        .then(cities => cities.json())
        .catch(this.handleError)
    }

    private handleError(e) {
        console.error("An error occurred", e);
        return Promise.reject(e.message || e);
      }

}