import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { environment } from '../../environments/environment';
import { IDeal } from '../models/deal';


@Injectable()
export class DealService {
    private _apiUrl = `${environment.apiDomain}/deals`;
    private _header = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http){}

    GetDeals(): Promise<IDeal[]>{
        return this.http.get(this._apiUrl)
        .toPromise()
        .then(deals => deals.json() as IDeal[])
        .catch(this.handleError)
    }

    GetDealById(id: number): Promise<IDeal>{
        return this.http.get(`${this._apiUrl}/${id}`)
            .toPromise()
            .then(deal => deal.json())
            .catch(this.handleError);
    }

    CreateDeal(deal: IDeal){
        return this.http.post(
            this._apiUrl, 
            JSON.stringify(deal),
            {headers: this._header}
        )
        .toPromise()
        .catch(this.handleError);
    }

    DeleteDeal(deal: IDeal){
        return this.http.delete(`${this._apiUrl}/${deal.id}`)
            .toPromise()
            .catch(this.handleError);
    }

    private handleError(e){
        console.error('An error occurred', e);
        return Promise.reject(e.message || e);
    }
}