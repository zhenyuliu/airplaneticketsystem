import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Http, Headers, URLSearchParams, RequestOptions, Response } from "@angular/http";
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ITicket } from "app/models/ticket";
import { ISearchTerm } from "app/models/searchTerms";



@Injectable()
export class TicketService {
  private _apiUrl = `${environment.apiDomain}/tickets`;
  private _header = new Headers({ "Content-Type": "application/json" });
  private _options: RequestOptions;

  constructor(private http: Http) {}

  getTickets(): Promise<ITicket[]> {
    return this.http
      .get(this._apiUrl)
      .toPromise()
      .then(t => t.json() as ITicket[])
      .catch(this.handleError);
  }

  getTicketById(id: number): Promise<ITicket>{
    return this.http.get(`${this._apiUrl}/${id}`)
    .toPromise()
    .then(t => t.json())
    .catch(this.handleError);
  }

  getTicketsByParamTerm(searchTerms: ISearchTerm) {
    return this.http.get(this._apiUrl, { params: searchTerms})
    .toPromise()
    .then(tickets => tickets.json())
    .catch(this.handleError);
  }

  updateTicket(ticket: ITicket){
    return this.http.put(`${this._apiUrl}/${ticket.id}`, JSON.stringify(ticket), {headers: this._header})
    .toPromise()
    .catch(this.handleError);
  }

  private toHttpParams(searchTerms: ISearchTerm, searchParams: URLSearchParams){
    for (var key in searchTerms) {
      if (searchTerms.hasOwnProperty(key)) {
        let val = searchTerms[key];
        searchParams.set(key, val);
      }
    }
  }

  private extractData(res: Response) {
    let body = res.json();
          return body;
      }

  private handleError(e) {
    console.error("An error occurred", e);
    return Promise.reject(e.message || e);
  }
}